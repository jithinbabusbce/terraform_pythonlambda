##Lambda to execute at 8am UTC
resource "aws_lambda_function" "wget_lambda" {
  filename      = "lambda_handler.zip"
  function_name = var.lambda8am
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "lambda_handler.lambda_handler"

  source_code_hash = filebase64sha256("lambda_handler.zip")

  runtime = "python3.8"
}

##Lambda to execute at 10PM UTC
resource "aws_lambda_function" "wget_lambda_10" {
  filename      = "lambda_handler.zip"
  function_name = var.lambda10pm
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "lambda_handler.lambda_handler"

  source_code_hash = filebase64sha256("lambda_handler.zip")

  runtime = "python3.8"
}

