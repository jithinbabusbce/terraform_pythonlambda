## Event rule for 8AM
resource "aws_cloudwatch_event_rule" "rule" {
  name                = var.lambda8am
  schedule_expression = var.schedule_expression
}

## Event rule for 10PM
resource "aws_cloudwatch_event_rule" "rule10" {
  name                = var.lambda10pm
  schedule_expression = var.schedule_expression10
}

##Target for 8AM
resource "aws_cloudwatch_event_target" "target" {
  rule = aws_cloudwatch_event_rule.rule.name
  arn  = aws_lambda_function.wget_lambda.arn
}

##Target for 10PM
resource "aws_cloudwatch_event_target" "target10" {
  rule = aws_cloudwatch_event_rule.rule10.name
  arn  = aws_lambda_function.wget_lambda_10.arn
}

## Lmbda permission at 8AM
resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.wget_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.rule.arn
}


## Lmbda permission at 10PM
resource "aws_lambda_permission" "allow_cloudwatch10" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.wget_lambda_10.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.rule10.arn
}
