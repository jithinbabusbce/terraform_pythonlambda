variable "AWS_REGION" {
  description = "Region everything is executed in."
}

variable "role_name" {
  description = "IAM Role name"
}

variable "policy_name" {
  description = "IAM Policy name"
}


variable "lambda8am" {
  description = "Lambda name"
}

variable "lambda10pm" {
  description = "Lambda name"
}

variable "schedule_expression" {
  type        = string
  description = "AWS Schedule Expression 8AM UTC time"
}

variable "schedule_expression10" {
  type        = string
  description = "AWS Schedule Expression 10PM UTC time"
}
