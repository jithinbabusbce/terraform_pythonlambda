# Steps to follow for template deployment

Note: Follow creation and deletion steps mentioned in the readme file

### Prerequisites
1. terraform
2. AWS authentication in terminal
   ` aws configure `


### For Running and creating the resources
      ` terraform init `
      ` terraform apply -var-file=terraform.tfvars  -auto-approve `

## variables file name
   ` variables.tf  `


## For destroying all resources created
` terraform destroy -var-file=terraform.tfvars -auto-approve `

